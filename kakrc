# Ada's Kakoune Config

colorscheme default

# MacOS Clipboard Integration
hook global NormalKey y|d|c %{ nop %sh{
    printf %s "$kak_reg_dquote" | pbcopy
}}

map global normal P '!pbpaste<ret>'
map global normal p '<a-!>pbpaste<ret>'

# Fuzzy Finders
def -docstring 'invoke fzf to open a file' \
  fzf-file %{ %sh{
    if [ -z "$TMUX" ]; then
      echo echo only works inside tmux
    else
      FILE=$(find * -type f | fzf-tmux -d 15)
      if [ -n "$FILE" ]; then
        printf 'eval -client %%{%s} edit %%{%s}\n' "${kak_client}" "${FILE}" | kak -p "${kak_session}"
      fi
    fi
} }

def -docstring 'invoke fzf to select a buffer' \
  fzf-buffer %{ %sh{
    if [ -z "$TMUX" ]; then
      echo echo only works inside tmux
    else
      BUFFER=$(printf %s\\n "${kak_buflist}" | tr : '\n' | fzf-tmux -d 15)
      if [ -n "$BUFFER" ]; then
        echo "eval -client '$kak_client' 'buffer ${BUFFER}'" | kak -p ${kak_session}
      fi
    fi
} }

# Keymap
map -docstring 'Fuzzy find file'   global user f ':fzf-file<ret>'
map -docstring 'Fuzzy find buffer' global user b ':fzf-buffer<ret>'
map -docstring 'Split horizontal'  global user s ':tmux-new-horizontal<ret>'
map -docstring 'Open terminal'     global user t ':tmux-repl-horizontal<ret>'
